import * as React from 'react';

class App extends React.Component {
  public render() {
    return (
      <div className="App">Hello world!</div>
    );
  }
}

export default App;
