import createHistory from 'history/createBrowserHistory';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import 'sanitize.css/sanitize.css';
import configureStore from './configureStore';
import App from './containers/App';

import registerServiceWorker from './registerServiceWorker';

// Create redux store with history
const initialState = {};
const history = createHistory();
const store = configureStore(initialState);

const MOUNT_NODE = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
  </Provider>,
  MOUNT_NODE
);
registerServiceWorker();
