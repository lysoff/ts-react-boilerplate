/**
 * Create the store with dynamic reducers
 */


// import { fromJS } from 'immutable';
import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { fromJS } from 'immutable';
import createReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(initialState = {}) {
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  const middlewares = [
    sagaMiddleware,
  ];


  const enhancers = [
    applyMiddleware(...middlewares),
  ];

  const windowIfDefined = typeof window === 'undefined' ? null : window as any;

  const composeEnhancers = windowIfDefined.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  // TODO: `any` for store may be not the best idea
  const store = createStore(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(...enhancers)
  ) as any;


  // Extensions
  store.runSaga = sagaMiddleware.run;
  store.injectedReducers = {} as any; // Reducer registry
  store.injectedSagas = {}; // Saga registry

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(createReducer(store.injectedReducers));
    });
  }

  return store;
}
