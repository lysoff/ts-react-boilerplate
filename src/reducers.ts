/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux-immutable';

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer(injectedReducers?: any): any {
  return combineReducers({
    global: (state: any = {}) =>{ console.log(state); return state },
    ...injectedReducers,
  }) as any;
}